from django.conf.urls import include, url
from django.contrib import admin

from vote import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^accounts/', include('registration.backends.hmac.urls'), name='login'),
    url(r'^', include('vote.urls')),

]