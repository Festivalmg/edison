from django.contrib.auth.decorators import login_required
from django.shortcuts import render
import datetime


@login_required(login_url='login')
def vote_second(request: 'request') -> render:
    return render(request, 'vote_second.html')


@login_required(login_url='login')
def result(request: 'request') -> render:
    return render(request, 'vote.html')


@login_required(login_url='login')
def new_vote(request: 'request') -> render:
    return render(request, 'vote.html')


@login_required(login_url='login')
def new_vote(request: 'request') -> render:
    now = datetime.datetime.now()

    time = (i for i in range(int(now.hour), 24))
    """New Vote"""
    return render(request, 'vote_first.html', {'time': time})


def login(request: 'request') -> render:
    """Login view"""
    return render(request, 'registration/login.html')


@login_required(login_url='login')
def vote(request: 'request') -> render:
    """Index view"""
    return render(request, 'vote.html')


