from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.vote, name='vote'),
    url(r'^new_vote/', views.new_vote, name='new_vote'),
    url(r'^vote_second/', views.vote_second, name='vote_second'),
    url(r'^result/', views.result, name='result'),
]