from django.contrib.auth.models import User
from django.forms import ModelForm
from django import forms


class VoteForm(forms.Form):
    d_first = forms.IntegerField(label='Продолжительность первой фазы (в минутах)')
    d_second = forms.IntegerField(label='Продолжительность первой фазы (в минутах)')


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password')
